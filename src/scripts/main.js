var app = new Vue({
  el: "#noticias",
  data: {
    noticias: []
  },
  created: function () {
    var _this = this;

    jQuery.ajax({
      method: 'POST',
      crossDomain: true,
      url: 'http://localhost/noticias',
      complete: function (xhr, status) {
        response = JSON.parse(xhr.responseText);
        _this.noticias = response;
      }
    });
  }
});

$(document).ready(function () {

  if (!!document.querySelector('#grafico')) {
    var ctx = $("#grafico");

    jQuery.ajax({
      method: 'POST',
      crossDomain: true,
      url: 'http://localhost/grafico',
      complete: function (xhr, status) {
        response = JSON.parse(xhr.responseText);

        var values = [];
        var labels = [];

        for (var i = 0; i < response.length; i++) {
          values.push(response[i].aceitacaos);
          labels.push(response[i].nucleo);
        }

        var SantriGrafico = new Chart(ctx, {
          data: {
            labels: labels,
            datasets: [{
              data: values,
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)'
              ],
              borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
              ],
              borderWidth: 1
            }]
          },
          type: 'polarArea'
        });
      }
    });

  }

  if (!!document.querySelector('#globalHeader')) {
    affixHeader();
  }

  if (!!document.querySelector('#mobileMenu')) {
    mobileMenu();
  }

  if (!!document.querySelector('#formContato')) {
    $('#formContato').validate({
      debug: false,
      errorClass: "error",
      errorElement: "div",
      onkeyup: false,
      rules: {
        nome: {
          required: true
        },
        sobrenome: {
          required: true
        },
        email: {
          required: true,
          email: true
        },
        mensagem: {
          required: true
        },
        captcha: {
          required: true
        }
      },
      submitHandler: function (form) {
        $.ajax({
          url: "http://localhost/contato",
          type: "POST",
          data: new FormData($(form)),
          cache: false,
          processData: false,
          success: function (data) {
            console.log(data);
            if (data == "sucesso") {
              $("#formContato").fadeOut(400);
              $("#btnSubmit").attr('disabled', true);
              setTimeout(function () {
                $(".msg-success").removeClass('invisible');
              }, 400);
            }
          }
        });
        return false;
      }
    });
  }

  if (!!document.querySelector('.btnShowMore')) {
    $('.solucoes .btnShowMore').click(function (event) {
      event.preventDefault();
      var target = $.attr(this, 'data-target');
      $("#solucoes").css("background-image", "url(" + target + ")");
      $(".solucao").removeClass("active");
      $(this).closest(".solucao").addClass("active");

      return false;
    });
  }
  if (!!document.querySelector('.scrollTo')) {
    var $root = $('html, body');

    $('.scrollTo').click(function (event) {
      event.preventDefault();
      var href = $.attr(this, 'href');
      var gb_H = $('#globalHeader').outerHeight();
      console.log(Number(gb_H))
      if ($('body').hasClass('-affix-header')) {
        $root.animate({
          scrollTop: $(href).offset().top - Number(gb_H)
        }, 500);
      } else {
        $root.animate({
          scrollTop: $(href).offset().top
        }, 500);
      }

      return false;
    });
  }

  // Formulários
  //==========================================================================

  $.extend($.validator.messages, {
    required: 'Este campo &eacute; requerido.',
    remote: 'Por favor, corrija este campo.',
    email: 'Por favor, forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.',
    url: 'Por favor, forne&ccedil;a uma URL v&aacute;lida.',
    date: 'Por favor, forne&ccedil;a uma data v&aacute;lida.',
    dateISO: 'Por favor, forne&ccedil;a uma data v&aacute;lida (ISO).',
    number: 'Por favor, forne&ccedil;a um n&uacute;mero v&aacute;lido.',
    digits: 'Por favor, forne&ccedil;a somente d&iacute;gitos.',
    creditcard: 'Por favor, forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.',
    equalTo: 'Por favor, forne&ccedil;a o mesmo valor novamente.',
    accept: 'Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.',
    maxlength: jQuery.validator.format('Por favor, forne&ccedil;a n&atilde;o mais que {0} caracteres.'),
    minlength: jQuery.validator.format('Por favor, forne&ccedil;a ao menos {0} caracteres.'),
    rangelength: jQuery.validator.format('Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento.'),
    range: jQuery.validator.format('Por favor, forne&ccedil;a um valor entre {0} e {1}.'),
    max: jQuery.validator.format('Por favor, forne&ccedil;a um valor menor ou igual a {0}.'),
    min: jQuery.validator.format('Por favor, forne&ccedil;a um valor maior ou igual a {0}.')
  });

  var BRMask = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    BRMaskOptions = {
      onKeyPress: function (val, e, field, options) {
        field.mask(BRMask.apply({}, arguments), options);
      }
    };

  $('input.telefone').mask(BRMask, BRMaskOptions);

  function styledSelect() {
    if (!!document.querySelector('select')) {
      $('select').each(function () {
        if ($(this).parent().hasClass("styled-select")) {} else {
          classes = {};
          $($(this).attr('class').split(' ')).each(function () {
            if (this !== '') {
              classes[this] = this;
            }
          });
          $(this).wrap('<div class="styled-select" role="presentation"></div>');
          for (var class_name in classes) {
            $(this).closest('.styled-select').addClass(class_name);
          }
        }
      });
    }
  }

  function formErrors() {
    if (!!document.querySelector('input.error')) {
      $("body").on("focus", "input.error, select.error, textarea.error", function () {
        $(this).removeClass('error');
      });
    }
    if (typeof ($('input.success')) !== 'undefined' && $('input.success') !== null) {
      $("body").on("focus", "input.success, select.success, textarea.success",
        function () {
          $(this).removeClass('success');
        });
    }
  }


  // @Header Affix - Animação da header da página.
  //==========================================================================
  function affixHeader() {
    if (!!document.querySelector('#globalHeader')) {
      setTimeout(function () {
        var gb_H = $('#globalHeader').outerHeight();
        animeHeader(gb_H);
        $(window).scroll(function () {
          var gb_H_new = $('#globalHeader').outerHeight();
          animeHeader(gb_H, gb_H_new);
        });
      }, 100);
    } else {
      return;
    }
  }

  function animeHeader(gb_H, gb_H_new) {
    var scroll = getCurrentScroll();

    if ($('body').is('.-affix-header')) {
      $('#globalHeader').addClass('affix-header');

      if (scroll > $('.hero').outerHeight()) {
        $('#globalHeader').addClass('fixed');
      } else {
        $('#globalHeader').removeClass('fixed');
      }
    }
  }

  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }


  // @Mobile Menu
  //==========================================================================

  function mobileMenu() {
    var overlay = $('<div class="overlay overlay--mobile-menu" hidden></div>').attr('id', 'overlayMobileMenu').insertAfter('#globalHeader');
    var dragTarget = $('<div class="drag-target" hidden></div>').attr('id', 'dragTarget').insertAfter('#mobileMenu');
    if ($('[data-mobilemenu-content]')) {
      $('[data-mobilemenu-content]').each(function () {
        if ($(this).attr('data-mobilemenu-content') == 'top') {
          $('#mobileMenuContainer').prepend($(this).clone());
        } else {
          $('#mobileMenuContainer').append($(this).clone());
        }
      });
    }
    $(document).delegate('a[href="#mobileMenu"]', "click", function (e) {
      $('#mobileMenu').toggleClass('-open');
      $('html').toggleClass('-open');
      $('#overlayMobileMenu').fadeToggle(200);
    });
    $(document).on('keydown', function (e) {
      if (e.keyCode === 27 && $('#mobileMenu').hasClass('-open')) {
        closeMobileMenu();
      }
    });
    //$('#mobileMenuContainer').find('[data-grid]').removeAttr("data-grid");
    //$('#mobileMenuContainer').find('[data-atomic]').removeAttr("data-atomic");
    //$('#mobileMenuContainer').find('.gutter-m, .grid-inline, .grid-va-m').removeClass("gutter-m grid-inline grid-va-m");
    $('#mobileMenu #mobileMenuContainer ul').each(function () {
      $(this).addClass('list-mobile cf');
      if ($(this).children('li').hasClass('mega-menu')) {
        _width = $('.global-header .grid:not(.grid-inline)').outerWidth();
        $(this).find('.dropdown').width(_width);
      }
    });
    /* @Menu interno */
    if (typeof ($('#mobileMenu .list-dropdown')) !== 'undefined' && $('#mobileMenu .list-dropdown') !== null) {
      setTimeout(function () {
        mobileMenuInterno();
      }, 1000);
    }
  }

  function openMobileMenu() {
    $('#mobileMenu').addClass('-open');
    $('html').addClass('-open');
    $('#overlayMobileMenu').fadeIn(260);
  }

  function closeMobileMenu() {
    $('#mobileMenu').removeClass('-open');
    $('html').removeClass('-open');
    $('#overlayMobileMenu').fadeOut(260);
  }

  function mobileMenuInterno() {
    $('#mobileMenu .list-dropdown').each(function () {
      var item = $(this);
      var trigger = item.children('a');
      var list = item.children('ul');
      var bc = $('#mobileMenu').attr('data-bc');
      list.wrapAll('<div class="mobile-menu--layer ' + bc + '"></div>').addClass(bc);
      list.addClass('mobile-menu--layer--list').attr('data-atomic', '');
      item.addClass('trigger-layer');
      var layer = list.closest('.mobile-menu--layer');
      var btnBack = $('<div class="mobile-menu--header"><span class="btn-back mobile-menu--layer--btn-back"><i class="fa fa-angle-left" data-atomic="va-b fs-xl mr-xs"></i>Voltar</span></div>').prependTo(layer);
      var back = layer.find('.btn-back');
      trigger.click(function (e) {
        if ($(window).width() < 1024) {
          e.preventDefault();
        }
        layer.addClass('visible');
        $('#mobileMenu').addClass('mobile-menu--layer-visible');
      });
      back.click(function () {
        layer.removeClass('visible');
        $('#mobileMenu').removeClass('mobile-menu--layer-visible');
      });
    });
  }

});

var ctx = document.getElementById("myChart").getContext('2d');

var randomScalingFactor = function () {
  return Math.round(Math.random() * 100);
};

var chartColors = window.chartColors;
var color = Chart.helpers.color;
var config = {
  data: {
    datasets: [{
      data: [
        randomScalingFactor(),
        randomScalingFactor(),
        randomScalingFactor(),
        randomScalingFactor(),
        randomScalingFactor(),
      ],
      backgroundColor: [
        color(chartColors.red).alpha(0.5).rgbString(),
        color(chartColors.orange).alpha(0.5).rgbString(),
        color(chartColors.yellow).alpha(0.5).rgbString(),
        color(chartColors.green).alpha(0.5).rgbString(),
        color(chartColors.blue).alpha(0.5).rgbString(),
      ],
      label: 'My dataset' // for legend
    }],
    labels: [
      'Red',
      'Orange',
      'Yellow',
      'Green',
      'Blue'
    ]
  },
  options: {
    responsive: true,
    legend: {
      position: 'right',
    },
    title: {
      display: true,
      text: 'Chart.js Polar Area Chart'
    },
    scale: {
      ticks: {
        beginAtZero: true
      },
      reverse: false
    },
    animation: {
      animateRotate: false,
      animateScale: true
    }
  }
};

window.onload = function () {
  var ctx = document.getElementById('chart-area');
  window.myPolarArea = Chart.PolarArea(ctx, config);
};

document.getElementById('randomizeData').addEventListener('click', function () {
  config.data.datasets.forEach(function (piece, i) {
    piece.data.forEach(function (value, j) {
      config.data.datasets[i].data[j] = randomScalingFactor();
    });
  });
  window.myPolarArea.update();
});

var colorNames = Object.keys(window.chartColors);
document.getElementById('addData').addEventListener('click', function () {
  if (config.data.datasets.length > 0) {
    config.data.labels.push('data #' + config.data.labels.length);
    config.data.datasets.forEach(function (dataset) {
      var colorName = colorNames[config.data.labels.length % colorNames.length];
      dataset.backgroundColor.push(window.chartColors[colorName]);
      dataset.data.push(randomScalingFactor());
    });
    window.myPolarArea.update();
  }
});
document.getElementById('removeData').addEventListener('click', function () {
  config.data.labels.pop(); // remove the label first
  config.data.datasets.forEach(function (dataset) {
    dataset.backgroundColor.pop();
    dataset.data.pop();
  });
  window.myPolarArea.update();
});