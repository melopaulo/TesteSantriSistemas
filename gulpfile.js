//npm install --save-dev gulp open gulp-load-plugins gulp-sass gulp-csso gulp-size jshint gulp-jshint gulp-uglify gulp-cache gulp-imagemin gulp-clean gulp-connect gulp-watch gulp-autoprefixer gulp-htmlmin gulp-rename
var gulp = require('gulp');
var open = require('open');
var concat = require('gulp-concat');
// Load plugins
var $ = require('gulp-load-plugins')();

// Browser support
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

// Styles
gulp.task('styles', function () {
  return gulp.src(['./src/styles/main.scss'])
    .pipe($.sass({
      outputStyle: 'expanded',
      precision: 10,
      // outputStyle: 'compressed',
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe($.autoprefixer({
      browsers: AUTOPREFIXER_BROWSERS
    }))
    // .pipe($.csso())
    .pipe($.rename('styles.css'))
    .pipe($.rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./dist/css'))
    .pipe($.connect.reload())
    .pipe($.size());
});

// Scripts
gulp.task('scripts', function () {
  return gulp.src(['./src/scripts/**/*.js'])
    .pipe($.jshint())
    .pipe($.uglify())
    .pipe(gulp.dest('./dist/js'))
    .pipe($.connect.reload())
    .pipe($.size());
});

// HTML
gulp.task('html', ['styles', 'scripts'], function () {
  return gulp.src('./src/*.html')
    .pipe($.htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./dist'))
    .pipe($.connect.reload())
    .pipe($.size());
});

// Images
gulp.task('images', function () {
  return gulp.src('./src/images/*')
    .pipe($.cache($.imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 5,
      svgoPlugins: [{
        removeViewBox: true
      }]
    })))
    .pipe(gulp.dest('./dist/img'))
    .pipe($.connect.reload())
    .pipe($.size());
});

// Data
gulp.task('data', function () {
  return gulp.src([
      './src/scripts/**/*.csv',
      './src/scripts/**/*.json'
    ])
    .pipe(gulp.dest('./dist/scripts'))
    .pipe($.connect.reload())
    .pipe($.size());
});

// Fonts
gulp.task('fonts', function () {
  return gulp.src('./src/fonts/**/*.*')
    .pipe($.connect.reload())
    .pipe(gulp.dest('./dist/fonts/'));
});

// Clean
gulp.task('clean', function () {
  return gulp.src(['./dist/css', './dist/js', './dist/img'], {
    read: false
  }).pipe($.clean());
});

// Build
gulp.task('build', ['html', 'images', 'data', 'fonts', 'watch']);

// Default task
gulp.task('default', ['clean'], function () {
  gulp.start('build');
});

// Connect
gulp.task('connect', function () {
  $.connect.server({
    root: './dist',
    port: 3000,
    livereload: true
  });
});

// Open
gulp.task('serve', ['connect'], function () {
  open("http://localhost:3000");
});

// Watch
gulp.task('watch', ['connect', 'serve'], function () {
  gulp.watch('./src/*.html', ['html']);
  gulp.watch('./src/styles/**/*.scss', ['styles']);
  gulp.watch('./src/scripts/**/*.js', ['scripts']);
  gulp.watch('./src/images/**/*', ['images']);
});